[
    'api': [
        gitRepoName: 'api',
        springAppName: 'api',
        dockerRegistoryName: 'api',
        port: 80,
        timezone: 'Asia/Tokyo',
    ],
    'id-api': [
        gitRepoName: 'id-api',
        springAppName: 'id-api',
        dockerRegistoryName: 'id-api',
        port: 9001,
        timezone: 'Asia/Tokyo',
    ],
    'id-web': [
        gitRepoName: 'id-web',
        springAppName: 'id-web',
        dockerRegistoryName: 'id-web',
        port: 8000,
        timezone: 'Asia/Tokyo',
    ],
    'myid-web': [
        gitRepoName: 'myid-web',
        springAppName: 'myid-web',
        dockerRegistoryName: 'myid-web',
        port: 8001,
        timezone: 'Asia/Tokyo',
    ],
    'search-web': [
        gitRepoName: 'search-web',
        springAppName: 'search-web',
        dockerRegistoryName: 'search-web',
        port: 8080,
        timezone: 'Asia/Tokyo',
        javaOpts: '"-Xms3g -Xmx3g -Xmn1g -XX:CMSInitiatingOccupancyFraction=80 -verbose:gc -XX:+PrintGCDetails -XX:+UseConcMarkSweepGC"',
    ],
    'product-api2': [
        gitRepoName: 'product-api2',
        springAppName: 'product-api',
        dockerRegistoryName: 'product-api',
        port: 8081,
        timezone: 'Asia/Tokyo',
        javaOpts: '"-Xms6g -Xmx6g -Xmn2g -XX:CMSInitiatingOccupancyFraction=80 -verbose:gc -XX:+PrintGCDetails -XX:+UseConcMarkSweepGC"',
    ],

    'ams-api': [
        gitRepoName: 'ams-api',
        springAppName: 'ams-api',
        dockerRegistoryName: 'ams-api',
        port: 8092,
        timezone: 'Asia/Tokyo',
    ],

    'ams-web': [
        gitRepoName: 'ams-web',
        springAppName: 'ams-web',
        dockerRegistoryName: 'ams-web',
        port: 8091,
        timezone: 'Asia/Tokyo',
    ],
   'mp-web': [
        gitRepoName: 'mp-web',
        springAppName: 'mp-web',
        dockerRegistoryName: 'mp-web',
        port: 8010,
        timezone: 'UTC',
    ],
   'buyer-web': [
        gitRepoName: 'buyer-web',
        springAppName: 'buyer-web',
        dockerRegistoryName: 'buyer-web',
        port: 8020,
        timezone: 'UTC',
    ],

]
