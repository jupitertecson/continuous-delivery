def verifyGitBranch(repo, branchName) {
    sshagent(['ssh-jenkins-key']) {
        sh """
        git ls-remote -h -t git@bitbucket.org:aperza/${repo}.git | sed -E 's#[a-zA-Z0-9]+\\s+refs/(heads|tags)/(.+)#\\2#' | grep -q ${branchName}
        """
    }
}

def buildApp(repo, branchName = 'master') {
    dir('common-spec') {
        checkout([
            $class: 'GitSCM',
            branches: [[name: branchName]],
            userRemoteConfigs: [[credentialsId: 'ssh-jenkins-key', url: "git@bitbucket.org:aperza/common-spec.git"]]
        ])
    }

    dir('common-csv') {
        checkout([
            $class: 'GitSCM',
            branches: [[name: branchName]],
            userRemoteConfigs: [[credentialsId: 'ssh-jenkins-key', url: "git@bitbucket.org:aperza/common-csv.git"]]
        ])
    }

    dir('common-db') {
        checkout([
            $class: 'GitSCM',
            branches: [[name: branchName]],
            userRemoteConfigs: [[credentialsId: 'ssh-jenkins-key', url: "git@bitbucket.org:aperza/common-db.git"]]
        ])
    }

    dir('common-web') {
        checkout([
            $class: 'GitSCM',
            branches: [[name: branchName]],
            userRemoteConfigs: [[credentialsId: 'ssh-jenkins-key', url: "git@bitbucket.org:aperza/common-web.git"]]
        ])
    }


    dir(repo) {
        checkout([
            $class: 'GitSCM',
            branches: [[name: branchName]],
            userRemoteConfigs: [[credentialsId: 'ssh-jenkins-key', url: "git@bitbucket.org:aperza/${repo}.git"]]
        ])
        sh './gradlew clean'
        sh './gradlew build'
    }
}

def buildDockerImage(buildDir, imageSpec) {
    dir(buildDir) {
        withCredentials([[ $class: 'AmazonWebServicesCredentialsBinding', accessKeyVariable: 'AWS_ACCESS_KEY_ID', credentialsId: 'aws-credential-aperza', secretKeyVariable: 'AWS_SECRET_ACCESS_KEY' ]]) {
            sh 'eval $(aws --region=us-east-1 ecr get-login)'
        }
        sh "docker build -t ${imageSpec} ."
        sh "docker push ${imageSpec}"
    }
}

def replaceContainer(imageSpec, springAppName, port, springProfile, timezone, javaOpts) {
    withCredentials([[ $class: 'AmazonWebServicesCredentialsBinding', accessKeyVariable: 'AWS_ACCESS_KEY_ID', credentialsId: 'aws-credential-aperza', secretKeyVariable: 'AWS_SECRET_ACCESS_KEY' ]]) {
        sh 'eval $(aws --region=us-east-1 ecr get-login)'
    }

    def envWithoutColor = env.DEPLOY_ENV - ~/\-(blue.*|green.*)/;
    def logOpts = "--log-driver=awslogs --log-opt awslogs-region=ap-northeast-1 --log-opt awslogs-group=aperza-api/${envWithoutColor} --log-opt awslogs-stream=${springAppName}"

    // not all service defines JAVA_OPTS
    def javaOptsKeyValue = (javaOpts != null && !javaOpts.isEmpty() ? "-e JAVA_OPTS=${javaOpts}" : "")

    sh """
    docker pull ${imageSpec}
    docker rm -f ${springAppName} || true
    docker run ${logOpts} --restart=always --name=${springAppName} -d -p ${port}:${port} -e TZ=${timezone} ${javaOptsKeyValue} -e EUREKA_SERVER_HOST=`hostname` -e SPRING_PROFILES_ACTIVE=${springProfile} -e WEBSERVICE_NAME=${springAppName} ${imageSpec}
    """
}

def cleanupOldImages() {
    try {
        withCredentials([[ $class: 'AmazonWebServicesCredentialsBinding', accessKeyVariable: 'AWS_ACCESS_KEY_ID', credentialsId: 'aws-credential-aperza', secretKeyVariable: 'AWS_SECRET_ACCESS_KEY' ]]) {
            sh 'eval $(aws --region=us-east-1 ecr get-login)'
        }
        sh '''
        docker rmi $(docker images | grep '<none>' | awk '{print $3}') || true
        '''
    } catch (err) {
        echo "Detect ignorable error: ${err}"
    }
}

def notifyHipChat(message, color = 'GRAY') {
    hipchatSend(
        color: color,
        message: message,
        notify: true,
        room: 'Aperza Project Jenkins',
        sendAs: 'Jenkins',
        server: 'aperza.hipchat.com',
        token: 'kXWHTbD5GMv7riZSEHGXLoHu5fSlstEZTVxoC7m8',
        v2enabled: true
    )
}

def notifySlack(message, channel = '#pd-aperza-web-builds') {
    slackSend(
        baseUrl: 'https://aperza.slack.com/services/hooks/jenkins-ci/',
        botUser: true,
        channel: channel,
        color: 'GRAY',
        failOnError: true,
        message: 'Hello World',
        teamDomain: 'aperza',
        token: 'hz7DypNl3vSKHkV5nasAX7VF'
    )
}

this

