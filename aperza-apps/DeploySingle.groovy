node {
// 設定値、共通スクリプト読み込み
def baseDir = pwd() + '/aperza-apps'
def conf = load "${baseDir}/conf/${env.DEPLOY_ENV}.groovy"
def service = load "${baseDir}/conf/service.groovy"
def modules = load "${baseDir}/modules.groovy"

try {
    // HipChat通知
    // modules.notifyHipChat("入力待機中: ${env.BUILD_TAG}")

    // パラメータ受け取り
    def inputParams = input(id: 'DeployParams', message: 'デプロイパラメータを指定', parameters: [
            [
                $class: 'ChoiceParameterDefinition',
                choices: service.keySet().join('\n'),
                description: 'デプロイするサービスを選択',
                name: 'deployTargetService'
            ],
            [
                $class: 'StringParameterDefinition',
                defaultValue: 'master',
                description: 'ビルドするGitブランチ名またはタグ名を入力',
                name: 'gitBranchName'
            ],
            [
                $class: 'BooleanParameterDefinition',
                defaultValue: false,
                description: 'Dockerイメージのlatestタグを今回ビルドするイメージに移動させる場合はチェック',
                name: 'updateDockerImageLatestTag'
            ]
        ])

    // HipChat通知
//    modules.notifyHipChat("""\
//        ${env.DEPLOY_ENV} 環境のデプロイを開始します。 ${env.BUILD_TAG}
//            / 対象サービス: ${inputParams.deployTargetService}
//            / gitブランチorタグ: ${inputParams.gitBranchName}
//            / Dockerイメージlatestタグ更新 ? ${inputParams.updateDockerImageLatestTag}
//    """.stripIndent())

    modules.notifySlack("""\
        Deploying ${env.DEPLOY_ENV} - ${env.BUILD_TAG}
            / Service: ${inputParams.deployTargetService}
            / Branch Name: ${inputParams.gitBranchName}
            / Update latest docker image ? ${inputParams.updateDockerImageLatestTag}
            / Build URL: (<${env.BUILD_URL}|Open>)
    """.stripIndent())


    echo "deployTargetService: ${inputParams.deployTargetService}"
    echo "gitBranchName: ${inputParams.gitBranchName}"
    echo "updateDockerImageLatestTag: ${inputParams.updateDockerImageLatestTag}"

    // Docker registory
    def dockerRegistry = '166055332606.dkr.ecr.us-east-1.amazonaws.com'

    // Dockerイメージ名
    def dockerImageName = "aperza/${service[inputParams.deployTargetService].dockerRegistoryName}"

    // Dockerイメージにつけるタグ
    def dockerTag = inputParams.gitBranchName.replace("/", "-")

    // Dockerイメージの完全名
    def dockerImageSpec = "${dockerRegistry}/${dockerImageName}:${dockerTag}"
    def dockerImageSpecLatest = "${dockerRegistry}/${dockerImageName}:latest"

    // ================
    // == メイン処理 ==
    // ================

    // docker build && push は test or stg 環境へのデプロイの場合のみ実行
    if (env.DEPLOY_ENV in ['mp-test', 'test', 'stg-blue', 'stg-green']) {
        node('aperza && !aperza-api-prod') {
            def gitRepoName = service[inputParams.deployTargetService].gitRepoName

            // -- STAGE: Gradleビルド
            stage 'build spring app'
//            modules.verifyGitBranch(gitRepoName, inputParams.gitBranchName)
//            modules.buildApp(gitRepoName, inputParams.gitBranchName)

            // -- STAGE: Dockerイメージのビルド, Dockerイメージのpush
            stage 'build docker image'
            modules.buildDockerImage(gitRepoName, dockerImageSpec)
            if (inputParams.updateDockerImageLatestTag) {
                modules.buildDockerImage(gitRepoName, dockerImageSpecLatest)
            }
//            modules.cleanupOldImages()
        }
    }

    // デプロイ対象スレーブで実行
    node(conf.slaveLabel) {
        def springAppName = service[inputParams.deployTargetService].springAppName
        def port = service[inputParams.deployTargetService].port
        def springProfile = conf.springProfile
        def timezone = service[inputParams.deployTargetService].timezone
        def javaOpts = service[inputParams.deployTargetService].javaOpts

        // -- STAGE: Dockerイメージをpull, 古いコンテナを破棄して新しくrun
        stage 'deploy container'
//        modules.replaceContainer(dockerImageSpec, springAppName, port, springProfile, timezone, javaOpts)

        // -- STAGE: 古いイメージの浄化
        stage 'cleanup old images'
//        modules.cleanupOldImages()
    }

    // HipChat通知
//    modules.notifyHipChat("デプロイが正常終了しました。 ${env.BUILD_TAG}", 'GREEN')
    modules.notifySlack("Deployment complete. ${env.BUILD_TAG}", 'GREEN')

} catch (InterruptedException e) {
    // HipChat通知
//    modules.notifyHipChat("デプロイがキャンセルされました。 ${env.BUILD_TAG}", 'YELLOW')
    modules.notifySlack("Build Failed: ${env.BUILD_TAG} (<${env.BUILD_URL}|Open>)", 'YELLOW')
} catch (e) {
    currentBuild.result = 'FAILURE'

    // HipChat通知
//    modules.notifyHipChat("@all デプロイが失敗しました。 ${env.BUILD_TAG}", 'RED')
    modules.notifySlack("@here Build Failed: ${env.BUILD_TAG} (<${env.BUILD_URL}|Open>)", 'YELLOW')
}
}
